<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

class SocialPlatformTwitter implements SocialPlatform
{
    protected $arguments = null;

    public function __construct($arguments)
    {
        if (empty((string)$arguments['handle'])) {
            throw new HoundrException('The twitter social platform needs a handle parameter');
        }

        $this->arguments = $arguments;
    }

    public function authorize_api()
    {
        // Nothing needed (currently we are just screen-scraping)
        return null;
    }

    public function latest_activity($auth_data, $filter = null)
    {
        $months = array(
            'Jan' => 1,
            'Feb' => 2,
            'Mar' => 3,
            'Apr' => 4,
            'May' => 5,
            'Jun' => 6,
            'Jul' => 7,
            'Aug' => 8,
            'Sep' => 9,
            'Oct' => 10,
            'Nov' => 11,
            'Dec' => 12,
        );

        $timestamps = array();
        $c = file_get_contents('https://twitter.com/' . (string)$this->arguments['handle']);
        $matches = array();
        $num_matches = preg_match_all('# class="tweet-timestamp[^"]*" title="(\d+):(\d+) (AM|PM) - (\d+) (\w\w\w) (\d+)"#', $c, $matches);
        for ($i = 0; $i < $num_matches; $i++) {
            $hour = intval($matches[1][$i]);
            $minute = intval($matches[2][$i]);
            if (($matches[3][$i] == 'PM') && ($hour != 12)) {
                $hour += 12;
            }
            $day = intval($matches[4][$i]);
            $month = $months[$matches[5][$i]];
            $year = intval($matches[6][$i]);

            $timestamp = mktime($hour, $minute, 0, $month, $day, $year);
            $timestamps[] = $timestamp;
        }

        sort($timestamps);

        return array_pop($timestamps);
    }

    public function oldest_flagged($auth_data, $filter = null)
    {
        throw new HoundrException('No flag check on twitter social platform');
    }
}
