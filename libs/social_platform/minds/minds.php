<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

// Hoping Minds will implement RSS support or a proper API
// Could try and base an unofficial PHP API off (or hook into Python) @ https://gitlab.com/granitosaurus/minds-api, but would take time

namespace Houndr;

class SocialPlatformMinds implements SocialPlatform
{
    protected $arguments = null;

    public function __construct($arguments)
    {
        if (empty((string)$arguments['handle'])) {
            throw new HoundrException('The minds social platform needs a handle parameter');
        }

        $this->arguments = $arguments;
    }

    public function authorize_api()
    {
        // TODO
        throw new HoundrException('The minds social platform is not yet implement');
    }

    public function latest_activity($auth_data, $filter = null)
    {
        // TODO
        throw new HoundrException('The minds social platform is not yet implement');
    }

    public function oldest_flagged($auth_data, $filter = null)
    {
        throw new HoundrException('No flag check on minds social platform');
    }
}
