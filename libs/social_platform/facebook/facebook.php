<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

class SocialPlatformFacebook implements SocialPlatform
{
    protected $arguments = null;

    public function __construct($arguments)
    {
        if (empty((string)$arguments['profile-id'])) {
            throw new HoundrException('The facebook social platform needs a profile-id parameter');
        }
        if (preg_match('#^(me|\d+)$#', (string)$arguments['profile-id']) == 0) {
            throw new HoundrException('The facebook social platform\'s profile-id parameter must be numeric');
        }
        if (empty((string)$arguments['app-id'])) {
            throw new HoundrException('The facebook social platform needs an app-id parameter');
        }
        if (preg_match('#^\d+$#'/*is_numeric will not work due to length*/, (string)$arguments['app-id']) == 0) {
            throw new HoundrException('The facebook social platform\'s app-id parameter must be numeric');
        }
        if (empty((string)$arguments['app-secret'])) {
            throw new HoundrException('The facebook social platform needs an app-secret parameter');
        }

        $this->arguments = $arguments;
    }

    public function authorize_api()
    {
        require_once(__DIR__ . '/Facebook/autoload.php');

        session_start();

        $fb = new \Facebook\Facebook(array(
            'app_id' => (string)$this->arguments['app-id'],
            'app_secret' => (string)$this->arguments['app-secret'],
        ));

        $helper = $fb->getRedirectLoginHelper();
        if (isset($_GET['code'])) {
            return $helper->getAccessToken()->getValue();
        }

        $protocol = ((!empty($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'on')) ? 'https' : 'http';
        $self_url = $protocol . '://' . $_SERVER['HTTP_HOST'];
        $self_url .= $_SERVER['REQUEST_URI'];

        $url = $helper->getLoginUrl($self_url, array('user_posts', 'manage_pages'));
        header('Location: ' .$url);
        exit();
    }

    public function latest_activity($auth_data, $filter = null)
    {
        if ($auth_data === null) {
            throw new HoundrException('You need to authorize the facebook social platform in a web browser, using oauth.php?platform_id=<platform-id>');
        }

        require_once(__DIR__ . '/Facebook/autoload.php');

        $fb = new \Facebook\Facebook(array(
            'app_id' => (string)$this->arguments['app-id'],
            'app_secret' => (string)$this->arguments['app-secret'],
            'default_access_token' => $auth_data,
        ));

        $times = array();
        $graph_path = '/' . (string)$this->arguments['profile-id'] . '/feed';
        $_posts = $fb->get($graph_path);
        $posts = $_posts->getDecodedBody();

        foreach ($posts['data'] as $post) {
            $times[] = strtotime($post['created_time']);
        }

        sort($times);

        return array_pop($times);
    }

    public function oldest_flagged($auth_data, $filter = null)
    {
        throw new HoundrException('No flag check on facebook social platform');
    }
}
