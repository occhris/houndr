<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

interface SocialPlatform
{
    public function authorize_api();

    public function latest_activity($auth_data, $filter = null);

    public function oldest_flagged($auth_data, $filter = null);
}
