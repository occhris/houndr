<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

class SocialPlatformEmail implements SocialPlatform
{
    protected $arguments = null;

    public function __construct($arguments)
    {
        if (empty((string)$arguments['imap-username'])) {
            throw new HoundrException('The email social platform needs a imap-username parameter');
        }
        if (empty((string)$arguments['imap-password'])) {
            throw new HoundrException('The email social platform needs a imap-password parameter');
        }
        if (empty((string)$arguments['imap-server'])) {
            throw new HoundrException('The email social platform needs a imap-server parameter');
        }
        if ((isset($arguments['imap-port'])) && (!is_numeric((string)$arguments['imap-port']))) {
            throw new HoundrException('The email social platform\'s imap-port parameter must be numeric');
        }
        if ((isset($arguments['imap-ssl'])) && (!in_array((string)$arguments['imap-ssl'], array('on', 'off')))) {
            throw new HoundrException('The email social platform\'s imap-ssl parameter must only be set to \'on\' or \'off\'');
        }

        $this->arguments = $arguments;
    }

    public function authorize_api()
    {
        // Nothing needed
        return null;
    }

    protected function check_env()
    {
        if (!function_exists('imap_open')) {
            throw new HoundrException('The PHP IMAP extension is required if you are using the email social platform');
        }
    }

    protected function get_connection()
    {
        $server = (string)$this->arguments['imap-server'];
        $port = empty((string)$this->arguments['imap-port']) ? '143' : (string)$this->arguments['imap-port'];
        $username = (string)$this->arguments['imap-username'];
        $password = (string)$this->arguments['imap-password'];
        $ssl = empty((string)$this->arguments['imap-ssl']) ? false : ((string)$this->arguments['imap-ssl'] == 'on');
        $ref = '{' . $server . ':' . $port . '/imap/novalidate-cert' . ($ssl ? '/ssl' : '') . '}';
        $mbox = imap_open($ref, $username, $password);
        return array($mbox, $ref);
    }

    protected function get_qualifying_msgs($imap_filter_sup = '', $filter = null, $sent_only = false)
    {
        list($mbox, $ref) = $this->get_connection();

        $cutoff_days = intval(empty((string)$this->arguments['cutoff-days']) ? '365' : (string)$this->arguments['cutoff-days']); // Needed for performance
        $imap_filter = 'SINCE "' . date('j-M-Y', time() - 60 * 60 * 24 * $cutoff_days) . '"';
        if ($imap_filter_sup != '') {
            $imap_filter .= ' ' . $imap_filter_sup;
        }
        $msgs = array();

        $boxes = imap_list($mbox, $ref, '*');
        foreach ($boxes as $box) {
            if ($sent_only) {
                if (stripos($box, 'sent') === false) {
                    continue;
                }
            }

            imap_reopen($mbox, $box);

            if ($filter === null) {
                $_msgs = imap_search($mbox, ($imap_filter == '') ? 'ALL' : $imap_filter);

                if ($_msgs === false) {
                    $_msgs = array();
                }
                foreach ($_msgs as $_msg) {
                    $msgs[] = strtotime(imap_headerinfo($mbox, $_msg)->date);
                }
            } else {
                foreach (array_map('trim', explode(',', $filter)) as $f) {
                    $_imap_filter = $imap_filter . (($imap_filter == '') ? '' : ' ') . 'TO "' . str_replace('"', '', $f) . '"';

                    $_msgs = imap_search($mbox, $_imap_filter);
                    if ($_msgs === false) {
                        $_msgs = array();
                    }
                    foreach ($_msgs as $_msg) {
                        $msgs[] = strtotime(imap_headerinfo($mbox, $_msg)->date);
                    }
                }
            }
        }

        return $msgs;
    }

    public function latest_activity($auth_data, $filter = null)
    {
        $this->check_env();

        $msgs = $this->get_qualifying_msgs('', $filter, true);

        if (count($msgs) == 0) {
            return null;
        }

        sort($msgs);
        return array_pop($msgs);
    }

    public function oldest_flagged($auth_data, $filter = null)
    {
        $this->check_env();

        $msgs = $this->get_qualifying_msgs('FLAGGED', $filter);

        if (count($msgs) == 0) {
            return null;
        }

        sort($msgs);
        return array_shift($msgs);
    }
}
