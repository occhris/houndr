<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

class SocialPlatformRss implements SocialPlatform
{
    protected $arguments = null;

    public function __construct($arguments)
    {
        if (empty((string)$arguments['url'])) {
            throw new HoundrException('The rss social platform needs a url parameter');
        }

        $this->arguments = $arguments;
    }

    public function authorize_api()
    {
        // Nothing needed
        return null;
    }

    public function latest_activity($auth_data, $filter = null)
    {
        require_once(__DIR__ . '/rss-php/Feed.php');
        $rss = \Feed::loadRss((string)$this->arguments['url'], empty((string)$this->arguments['http-auth-user']) ? null : (string)$this->arguments['http-auth-user'], empty((string)$this->arguments['http-auth-password']) ? null : (string)$this->arguments['http-auth-password']);

        $timestamps = array();
        foreach ($rss->item as $item) {
            $timestamps[] = (integer)$item->timestamp;
        }

        sort($timestamps);

        return array_pop($timestamps);
    }

    public function oldest_flagged($auth_data, $filter = null)
    {
        throw new HoundrException('No flag check on rss social platform');
    }
}
