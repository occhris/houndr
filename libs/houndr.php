<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

ini_set('allow_url_fopen', 'On');
ini_set('display_errors', '1');
error_reporting(E_ALL);

spl_autoload_register(function($class_name) {
    if (substr($class_name, 0, 7) != 'Houndr\\') {
        return;
    }

    $class_name = substr($class_name, 7);

    $matches = array();

    $subdir = '';
    if (preg_match('#^SocialPlatform(.*)$#', $class_name, $matches) != 0) {
        $subdir = 'social_platform/' . strtolower($matches[1]);
        $class_name = $matches[1];

        require_once(__DIR__ . '/social_platform/social_platform.php'); // This will be needed for the interface
    }

    $filename = strtolower(ltrim(preg_replace('#([A-Z])#', '_$1', $class_name), '_')) . '.php';
    $path = __DIR__ . '/' . $subdir . (($subdir == '') ? '' : '/') . $filename;

    if (is_file($path)) {
        require_once($path);
    }
});

set_error_handler(function($err_severity, $err_msg, $err_file, $err_line, $err_context)
{
    if (error_reporting() === 0) {
        // Error was suppressed with the @-operator
        return false;
    }

    throw new \ErrorException($err_msg . ' in ' . $err_file . ' on line ' . strval($err_line));
});
