<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

class Engine
{
    public function authorize_apis($target_platform_id, $config_filename = 'config.xml')
    {
        $config = $this->load_config($config_filename);
        $state = $this->load_state();

        $social_platforms = $this->load_social_platforms($config);
        if (!isset($social_platforms[$target_platform_id])) {
            throw new HoundrException('Could not find social platform instance, ' . $target_platform_id);
        }

        $auth_data = $social_platforms[$target_platform_id]->authorize_api();
        $state['auth_data__' . $target_platform_id] = $auth_data;
        $this->save_state($state);
    }

    public function social_platform_data_dump($skip_platforms = null, $only_platforms = null, $only_part = null, $filter = null, $config_filename = 'config.xml')
    {
        $config = $this->load_config($config_filename);
        $state = $this->load_state();

        $social_platforms = $this->load_social_platforms($config);

        $data = array();
        foreach ($social_platforms as $platform_id => $social_platform) {
            if (($skip_platforms !== null) && (in_array($platform_id, $skip_platforms))) {
                continue;
            }

            if (($only_platforms !== null) && (!in_array($platform_id, $only_platforms))) {
                continue;
            }

            $auth_data = isset($state['auth_data__' . $platform_id]) ? $state['auth_data__' . $platform_id] : null;

            $arr = array();

            if (($only_part === null) || ($only_part === 1)) {
                try {
                    $latest_activity = $social_platform->latest_activity($auth_data, $filter);
                }
                catch (HoundrException $e) {
                    $latest_activity = $e->getMessage();
                }
                $arr['latest_activity'] = $latest_activity;
            }

            if (($only_part === null) || ($only_part === 2)) {
                try {
                    $oldest_flagged = $social_platform->oldest_flagged($auth_data, $filter);
                }
                catch (HoundrException $e) {
                    $oldest_flagged = $e->getMessage();
                }
                $arr['oldest_flagged'] = $oldest_flagged;
            }

            $data[$platform_id] = $arr;
        }
        return $data;
    }

    public function execute($skip_rules = null, $only_rules = null, $dry_run = false, $config_filename = 'config.xml')
    {
        $config = $this->load_config($config_filename);
        $state = $this->load_state();

        $this->debug_signal($config, 'STARTUP');

        $social_platforms = $this->load_social_platforms($config);
        $variables = $this->load_variables($config);

        try {
            $rules_and_times = $this->process_rules($config, $state, $social_platforms, $variables, $skip_rules, $only_rules, $dry_run);
        }
        catch (\Exception $ex) {
            // Send error e-mail
            $from = (string)$config->settings->email->from;
            $to = (string)$config->settings->email->to;
            $priority = 1;
            $subject = 'Houndr error';
            $body = $ex->getMessage();
            $headers = 'From: ' . $from . "\n" . 'Priority: ' . strval($priority);
            mail($to, $subject, $body, $headers);

            $this->debug_signal($config, $ex->getMessage(), true);

            throw $ex;
        }

        $this->debug_signal($config, 'SHUTDOWN');

        return $rules_and_times;
    }

    protected function load_config($config_filename)
    {
        $config_path = __DIR__ . '/../' . $config_filename;
        if (!is_file($config_path)) {
            throw new HoundrException('You need to create a ' . $config_filename . ' based upon config-sample.xml');
        }

        $config = simplexml_load_file($config_path);

        if (!isset($config->settings)) {
            return new HoundrException('No settings section configured');
        }
        if (isset($config->settings[1])) {
            return new HoundrException('Multiple setting sections configured');
        }

        if (!isset($config->settings->email)) {
            return new HoundrException('No email settings section configured');
        }
        if (isset($config->settings->email[1])) {
            return new HoundrException('Multiple email settings sections configured');
        }

        if (empty((string)$config->settings->email->to)) {
            return new HoundrException('No email to address email settings configured');
        }
        if (isset($config->settings->email->to[1])) {
            return new HoundrException('Multiple to address email settings configured');
        }
        if (empty((string)$config->settings->email->from)) {
            return new HoundrException('No email from address email settings configured');
        }
        if (isset($config->settings->email->from[1])) {
            return new HoundrException('Multiple from address email settings configured');
        }

        if (isset($config->settings->email->subjectPrefix)) {
            if (isset($config->settings->email->subjectPrefix[1])) {
                return new HoundrException('Multiple subjectPrefix email settings configured');
            }
        }
        if (isset($config->settings->email->subjectSuffix)) {
            if (isset($config->settings->email->subjectSuffix[1])) {
                return new HoundrException('Multiple subjectSuffix email settings configured');
            }
        }

        if (isset($config->settings->debug)) {
            if (isset($config->settings->debug[1])) {
                return new HoundrException('Multiple debug settings configured');
            }
            if (!in_array((string)$config->settings->debug, array('on', 'off'))) {
                throw new HoundrException('The email social platform\'s imap-tls parameter must only be set to \'on\' or \'off\'');
            }
        }

        if (isset($config->socialPlatforms[1])) {
            return new HoundrException('Multiple socialPlatforms sections configured');
        }

        if (!isset($config->rules)) {
            return new HoundrException('Missing rules section');
        }
        if (isset($config->rules[1])) {
            return new HoundrException('Multiple rules sections configured');
        }

        if (isset($config->variables[1])) {
            return new HoundrException('Multiple variables sections configured');
        }

        if (!empty((string)$config->settings->timezone)) {
            if (isset($config->settings->timezone[1])) {
                return new HoundrException('Multiple timezones configured');
            }

            date_default_timezone_set((string)$config->settings->timezone);
        }

        return $config;
    }

    protected function load_state()
    {
        $state_path = __DIR__ . '/../state.json';

        if (!is_file($state_path)) {
            $result = @file_put_contents($state_path, '');
            if ($result === false) {
                throw new HoundrException('You need to either set writable access to your Houndr directory, or create an empty state.json file with writable permissions');
            }
        }

        $state = @json_decode(file_get_contents($state_path), true);
        if (!is_array($state)) {
            $state = array();
        }
        return $state;
    }

    protected function load_social_platforms($config)
    {
        $social_platforms = array();

        if (!isset($config->socialPlatforms)) {
            return $variables;
        }

        foreach ($config->socialPlatforms->children() as $social_platform) {
            if (empty((string)$social_platform['platform-id'])) {
                return new HoundrException('Missing platform-id for social platform, ' . json_encode($social_platform,  JSON_PRETTY_PRINT));
            }

            $platform_id = (string)$social_platform['platform-id'];

            $social_platform_type = $social_platform->getName();
            $social_platform_class = '\Houndr\SocialPlatform' . ucfirst($social_platform_type);

            $social_platforms[$platform_id] = new $social_platform_class($social_platform);
        }

        return $social_platforms;
    }

    protected function load_variables($config)
    {
        $variables = array();

        if (!isset($config->variables)) {
            return $variables;
        }

        $timestamp = time();
        $current_date = date('Y-m-d', $timestamp);
        $current_year = intval(date('Y', $timestamp));

        foreach ($config->variables->set as $variable) {
            if (empty((string)$variable['name'])) {
                throw new HoundrException('Missing name for variable, ' . json_encode($variable,  JSON_PRETTY_PRINT));
            }

            $name = (string)$variable['name'];

            $_variable = (string)$variable;

            // Some variables may be date-bound, consider them blank if not inside date range
            if (isset($variable['start_date']) || isset($variable['end_date'])) {
                if (isset($variable['start_date'])) {
                    $start_date = (string)$variable['start_date'];
                } else {
                    $start_date = '*-01-01';
                }
                if (isset($variable['end_date'])) {
                    $end_date = (string)$variable['end_date'];
                } else {
                    $end_date = '*-01-01';
                }
                if ($start_date < $end_date) {
                    $start_date = str_replace('*', strval($current_year), $start_date);
                    $end_date = str_replace('*', strval($current_year), $end_date);
                } else {
                    $start_date = str_replace('*', strval($current_year), $start_date);
                    $end_date = str_replace('*', strval($current_year + 1), $end_date);
                }

                if ($current_date < $start_date || $current_date > $end_date) {
                    $variables[$name] = '';
                    continue;
                }
            }

            // Resolve references from other variables (only ones already loaded)
            foreach ($variables as $key => $val) {
                $_variable = str_replace('{' . $key . '}', $val, $_variable);
            }

            $_variable = $this->cleanup_formatting($_variable);

            $variables[$name] = $_variable;
        }

        return $variables;
    }

    protected function process_rules($config, &$state, $social_platforms, $variables, $skip_rules, $only_rules, $dry_run = false)
    {
        $rules_and_times = [];

        foreach ($config->rules->rule as $rule) {
            $rule_id = empty((string)$rule['id']) ? '' : (string)$rule['id'];

            if (($skip_rules !== null) && (in_array($rule_id, $skip_rules))) {
                continue;
            }

            if (($only_rules !== null) && (!in_array($rule_id, $only_rules))) {
                continue;
            }

            $rule_hash = empty((string)$rule['id']) ? md5(json_encode($rule,  JSON_PRETTY_PRINT)) : (string)$rule['id'];

            $time_to_run = $this->process_rule($config, $state, $rule, $social_platforms, $variables, $rule_hash, $dry_run);

            if ($time_to_run !== null) {
                $subject_inner = empty((string)$rule['email-subject']) ? 'nag-nag-nag' : (string)$rule['email-subject'];

                $body = trim(preg_replace('#^\s+#m', '', (string)$rule));
                foreach ($variables as $key => $val) {
                    $body = str_replace('{' . $key . '}', $val, $body);
                }

                $rules_and_times[] = array(
                    'subject' => $subject_inner,
                    'description' => $body,
                    'time' => $time_to_run,
                    'hash' => $rule_hash,
                );
            }
        }

        return $rules_and_times;
    }

    protected function process_rule($config, &$state, $rule, $social_platforms, $variables, $rule_hash, $dry_run = false)
    {
        $last_run = isset($state['rule_last_run__' . $rule_hash]) ? $state['rule_last_run__' . $rule_hash] : 0/*never*/;

        $has_trigger_time = isset($rule['trigger-time']);

        if (($dry_run) && (isset($rule['motivational'])) && ((string)$rule['motivational'] == '1')) {
            return null;
        }

        $time_to_run = null;

        $this->debug_signal($config, "Re-evaluating rule {$rule_hash}");

        // Already reached (we only trigger a rule once per day maximum)
        if (date('Y-m-d', $last_run) == date('Y-m-d', time())) {
            $this->debug_signal($config, " Rule failed (once per day)");
            return null; // Not triggered
        }

        if (isset($rule['trigger-daily-frequency'])) {
            $days = intval((string)$rule['trigger-daily-frequency']);
            if ($days <= 0) {
                throw new HoundrException('Invalid trigger-daily-frequency for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }

            $time_to_run = $last_run + 60 * 60 * 24 * $days + ($has_trigger_time ? (11 * 60 * 60)/*stop clock creep*/ : 0);
        }

        if (isset($rule['trigger-daily-frequency-range'])) {
            $matches = array();
            if (preg_match('#^(\d+)-(\d+)$#', (string)$rule['trigger-daily-frequency-range'], $matches) == 0) {
                throw new HoundrException('Invalid trigger-daily-frequency-range for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }

            if (isset($state['rule_days__' . $rule_hash])) {
                $days = $state['rule_days__' . $rule_hash];
            } else {
                $n1 = intval($matches[1]);
                $n2 = intval($matches[2]);
                if (($n1 <= 0) || ($n2 <= 0)) {
                    throw new HoundrException('Invalid trigger-daily-frequency-range for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
                }
                $days = mt_rand($n1, $n2);
            }
            $state['rule_days__' . $rule_hash] = $days;
            if (!$dry_run) {
                $this->save_state($state);
            }

            $time_to_run = $last_run + 60 * 60 * 24 * $days + ($has_trigger_time ? (11 * 60 * 60)/*stop clock creep*/ : 0);
        }

        if ($has_trigger_time) {
            $matches = array();
            if (preg_match('#^(\d\d?):(\d\d)$#', (string)$rule['trigger-time'], $matches) == 0) {
                throw new HoundrException('Invalid trigger-time for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }

            $hour = intval($matches[1]);
            $minute = intval($matches[2]);

            if ($time_to_run === null) {
                $time_to_run = mktime($hour, $minute, 0);
            } else {
                $time_to_run = mktime($hour, $minute, 0, intval(date('m'), $time_to_run), intval(date('j'), $time_to_run), intval(date('Y'), $time_to_run));
            }
        }

        if (isset($rule['trigger-days'])) {
            if ((string)$rule['trigger-days'] == '') {
                $this->debug_signal($config, " Rule failed (trigger-days blank)");
                return null; // Not triggered
            }

            $days = array_map('trim', explode(',' , (string)$rule['trigger-days']));
            $found_day = false;
            $_time_to_run = null;
            foreach ($days as $day) {
                $__time_to_run = strtotime($day, time());
                if (($_time_to_run === null) || ($_time_to_run > $__time_to_run)) {
                    $_time_to_run = $__time_to_run;
                }

                switch ($day) {
                    case 'Monday':
                        if (date('N') == '1') {
                            $found_day = true;
                        }
                        break;
                    case 'Tuesday':
                        if (date('N') == '2') {
                            $found_day = true;
                        }
                        break;
                    case 'Wednesday':
                        if (date('N') == '3') {
                            $found_day = true;
                        }
                        break;
                    case 'Thursday':
                        if (date('N') == '4') {
                            $found_day = true;
                        }
                        break;
                    case 'Friday':
                        if (date('N') == '5') {
                            $found_day = true;
                        }
                        break;
                    case 'Saturday':
                        if (date('N') == '6') {
                            $found_day = true;
                        }
                        break;
                    case 'Sunday':
                        if (date('N') == '7') {
                            $found_day = true;
                        }
                        break;
                    default:
                        throw new HoundrException('Non-recognised day, ' . $day .' for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
                }
            }

            if ($_time_to_run !== null) {
                if ($time_to_run === null) {
                    $time_to_run = $_time_to_run;
                } else {
                    $time_to_run = mktime(intval(date('H', $time_to_run)), intval(date('i', $time_to_run)), 0, intval(date('m', $_time_to_run)), intval(date('j', $_time_to_run)), intval(date('Y', $_time_to_run)));
                }
            }
        }

        // Not reached yet
        if (($time_to_run !== null) && ($time_to_run > time())) {
            $this->debug_signal($config, " Rule failed");
            return $time_to_run; // Not triggered
        }

        if (isset($rule['trigger-daily-inactivity'])) {
            $days = intval((string)$rule['trigger-daily-inactivity']);
            if ($days <= 0) {
                throw new HoundrException('Invalid trigger-daily-inactivity for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }

            // We already have triggered within $days - so we must wait a full nag-cycle
            if ($last_run > time() - 60 * 60 * 24 * $days - ($has_trigger_time ? (11 * 60 * 60)/*stop clock creep*/ : 0)) {
                $this->debug_signal($config, " Rule failed (trigger-daily-inactivity optimization)");
                return null; // Not triggered
            }

            $filter = empty((string)$rule['trigger-daily-inactivity-filter']) ? null : (string)$rule['trigger-daily-inactivity-filter'];

            if (empty((string)$rule['trigger-daily-inactivity-scope'])) {
                throw new HoundrException('Missing trigger-daily-inactivity-scope for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }
            $scope = (string)$rule['trigger-daily-inactivity-scope'];

            foreach (array_map('trim', explode(',', $scope)) as $platform_id) {
                if (!isset($social_platforms[$platform_id])) {
                    throw new HoundrException('Non-recognised social platform ' . $platform_id . ' for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
                }

                $auth_data = isset($state['auth_data__' . $platform_id]) ? $state['auth_data__' . $platform_id] : null;

                $latest_activity = $social_platforms[$platform_id]->latest_activity($auth_data, $filter);
                $latest_activity_adjusted = $latest_activity + $days * 60 * 60 * 24; // When there would be a problem by
                if (($latest_activity !== null) && ($latest_activity_adjusted > time())) {
                    $state['rule_last_run__' . $rule_hash] = $latest_activity_adjusted; // Fiddle the state, as we know we can avoid re-checking for a while
                    if (!$dry_run) {
                        $this->save_state($state);
                    }

                    $this->debug_signal($config, " Rule failed (trigger-daily-inactivity)");

                    return null; // Not triggered
                }
            }
        }

        if (isset($rule['trigger-flagged-threshold'])) {
            $hours = intval((string)$rule['trigger-flagged-threshold']);
            if ($hours <= 0) {
                throw new HoundrException('Invalid trigger-flagged-threshold for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }

            // We already have triggered within $hours - so we must wait a full nag-cycle)
            if ($last_run > time() - 60 * 60 * $hours) {
                $this->debug_signal($config, " Rule failed (trigger-flagged-threshold optimization)");
                return null; // Not triggered
            }

            $filter = empty((string)$rule['trigger-flagged-threshold-filter']) ? null : (string)$rule['trigger-flagged-threshold-filter'];

            if (empty((string)$rule['trigger-flagged-threshold-scope'])) {
                throw new HoundrException('Missing trigger-flagged-threshold-scope for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
            }
            $scope = (string)$rule['trigger-flagged-threshold-scope'];

            $oldest_flagged_adjusted = array();
            foreach (array_map('trim', explode(',', $scope)) as $platform_id) {
                if (!isset($social_platforms[$platform_id])) {
                    throw new HoundrException('Non-recognised social platform ' . $platform_id . ' for rule; ' . json_encode($rule,  JSON_PRETTY_PRINT));
                }

                $auth_data = isset($state['auth_data__' . $platform_id]) ? $state['auth_data__' . $platform_id] : null;

                $oldest_flagged = $social_platforms[$platform_id]->oldest_flagged($auth_data, $filter);
                if ($oldest_flagged !== null) {
                    $oldest_flagged_adjusted[] = $oldest_flagged + $hours * 60 * 60; // When there would be a problem by
                }
            }
            sort($oldest_flagged_adjusted);

            if ((count($oldest_flagged_adjusted) == 0) || ($oldest_flagged_adjusted[0] > time())) {
                $state['rule_last_run__' . $rule_hash] = (count($oldest_flagged_adjusted) == 0) ? time() : $oldest_flagged_adjusted[0]; // Fiddle the state, as we know we can avoid re-checking for a while
                if (!$dry_run) {
                    $this->save_state($state);
                }

                $this->debug_signal($config, " Rule failed (trigger-flagged-threshold)");

                return null; // Not triggered
            }
        }

        // If we got this far, the rule has passed and an email should be sent
        $this->debug_signal($config, " Rule passed", true);
        if (!$dry_run) {
            $this->dispatch_rule($config, $state, $rule, $rule_hash, $variables);
        }

        // Change state
        $state['rule_last_run__' . $rule_hash] = time();
        unset($state['rule_days__' . $rule_hash]); // So it will re-calculate next time
        if (!$dry_run) {
            $this->save_state($state);
        }

        return $time_to_run;
    }

    protected function dispatch_rule($config, &$state, $rule, $rule_hash, $variables)
    {
        $from = (string)$config->settings->email->from;
        $to = (string)$config->settings->email->to;

        $priority = empty((string)$rule['email-priority']) ? 3 : intval((string)$rule['email-priority']);
        if (($priority < 1) || ($priority > 5)) {
            $priority = 3;
        }

        $subject_inner = empty((string)$rule['email-subject']) ? 'nag-nag-nag' : (string)$rule['email-subject'];
        $subject_prefix = isset($config->settings->email->subjectPrefix) ? (string)$config->settings->email->subjectPrefix : '';
        $subject_suffix = isset($config->settings->email->subjectSuffix) ? (string)$config->settings->email->subjectSuffix : '';
        $subject = $subject_prefix . $subject_inner . $subject_suffix;

        $body = (string)$rule;

        $this->handle_incrementation($body, $rule_hash, $state);

        if (empty($body)) {
            $body = $subject_inner;
        } else {
            $body = $body;
        }

        foreach ($variables as $key => $val) {
            if ((strpos($subject, $key) !== false) || (strpos($body, $key) !== false)) {
                $this->handle_incrementation($val, $key, $state);

                $subject = str_replace('{' . $key . '}', $val, $subject);
                $body = str_replace('{' . $key . '}', $val, $body);
            }
        }

        $subject = $this->cleanup_formatting($subject);
        $body = $this->cleanup_formatting($body);

        $body .= "\n\n\n(Send-ID " . md5(uniqid('', true)) . ')';

        $headers = 'From: ' . $from . "\n" . 'Priority: ' . strval($priority);

        mail($to, $subject, $body, $headers);

        $this->debug_signal($config, "Sending email: " . var_export(array($to, $subject, $body, $headers), true));
    }

    protected function handle_incrementation(&$body, $rule_hash, &$state)
    {
        $matches = array();
        $num_matches = preg_match_all('#\{\+(\-?\d+)\}#', $body, $matches);
        for ($i = 0; $i < $num_matches; $i++) {
            $mem = 'increment__' . $rule_hash . '__' . $matches[0][$i];

            if (isset($state[$mem])) {
                $state[$mem] += intval($matches[1][$i]);
            } else {
                $state[$mem] = 1;
            }

            $body = str_replace($matches[0][$i], strval($state[$mem]), $body);
        }
    }

    protected function cleanup_formatting($text)
    {
        $text = preg_replace('#^\s*(.*)#m', '$1', $text); // Trim trailing white-space on individual lines
        $text = preg_replace('#^(.*)\s*$#Um', '$1', $text); // Trim leading white-space on individual lines
        $text = trim($text); // Trim leading/trailing lines
        return $text;
    }

    protected function is_debugging($config)
    {
        return (isset($config->settings->debug)) && ((string)$config->settings->debug == 'on');
    }

    function debug_signal($config, $message, $is_useful = false)
    {
        if ($this->is_debugging($config)) {
            echo "$message\n";
        }

        $log_path = __DIR__ . '/../houndr.log';
        if ($is_useful && is_file($log_path)) {
            $myfile = fopen($log_path, 'ab');
            fwrite($myfile, date('Y-m-d H:i:s') . " - $message\n");
            fclose($myfile);
        }
    }

    protected function save_state($state)
    {
        $state_path = __DIR__ . '/../state.json';

        ksort($state);

        $result = @file_put_contents($state_path, json_encode($state,  JSON_PRETTY_PRINT));
        if ($result === false) {
            throw new HoundrException('The state.json file is not writable');
        }
    }
}
