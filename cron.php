<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

try {
    require_once(__DIR__ . '/libs/houndr.php');

    if (php_sapi_name() != 'cli') {
        throw new HoundrException('Should be run with the PHP CLI client');
    }

    $opts = getopt('s:o:');
    $skip_rules = empty($opts['s']) ? null : explode(',', $opts['s']);
    $only_rules = empty($opts['o']) ? null : explode(',', $opts['o']);

    $engine = new Engine();
    $engine->execute($skip_rules, $only_rules);
}
catch (\Exception $ex) {
    echo "Exception:\n";
    echo $ex->getMessage();
}
