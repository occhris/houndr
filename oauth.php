<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

@header('Content-type: text/plain');

try {
    require_once(__DIR__ . '/libs/houndr.php');

    if (!isset($_GET['platform_id'])) {
        throw new HoundrException('platform_id parameter is needed');
    }

    $engine = new Engine();
    $engine->authorize_apis($_GET['platform_id']);

    echo 'Done';
}
catch (\Exception $ex) {
    echo "Exception:\n";
    echo $ex->getMessage();
}
