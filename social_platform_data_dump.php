<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

try {
    require_once(__DIR__ . '/libs/houndr.php');

    if (php_sapi_name() != 'cli') {
        throw new HoundrException('Should be run with the PHP CLI client');
    }

    $opts = getopt('s:o:p:f:');
    $skip_platforms = empty($opts['s']) ? null : explode(',', $opts['s']);
    $only_platforms = empty($opts['o']) ? null : explode(',', $opts['o']);
    $only_part = isset($opts['p']) ? intval($opts['p']) : null;
    $filter = isset($opts['f']) ? $opts['f'] : null;

    $engine = new Engine();
    $data = $engine->social_platform_data_dump($skip_platforms, $only_platforms, $only_part, $filter);
    print_r($data);
}
catch (\Exception $ex) {
    echo "Exception:\n";
    echo $ex->getMessage();
}
