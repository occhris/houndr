<?php

/**
 * Houndr
 *
 * @package Houndr
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

namespace Houndr;

header('Content-type: text/plain');

try {
    require_once(__DIR__ . '/libs/houndr.php');

    $skip_rules = empty($_GET['s']) ? null : explode(',', $_GET['s']);
    $only_rules = empty($_GET['o']) ? null : explode(',', $_GET['o']);

    $engine = new Engine();
    $rules_and_times = $engine->execute($skip_rules, $only_rules, true);
}
catch (\Exception $ex) {
    echo "Exception:\n";
    echo $ex->getMessage();
    exit();
}

//header('Content-type: text/calendar');
//header('Content-Disposition: attachment; filename="houndr-' . date('YmdHis') . '.ical"');

echo 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//houndr//NONSGML v1.0//EN
';

foreach ($rules_and_times as $details) {
    echo '
BEGIN:VEVENT
UID:' . $details['hash'] . '@' . $_SERVER['HTTP_HOST'] . '
DTSTAMP:' . date('Ymd') . 'T' . date('His') . 'Z
DTSTART:' . date('Ymd', $details['time']) . 'T' . date('His', $details['time']) . 'Z
DTEND:' . date('Ymd', $details['time']) . 'T' . date('His', $details['time']) . 'Z
SUMMARY:' . $details['subject'] . '
DESCRIPTION:' . str_replace("\n", '\n', $details['description']) . '
END:VEVENT
';
}

echo '
END:VCALENDAR
';
