# Houndr #

Houndr is an automated personal assistant, designed to help technical people be effective when burdened by a large number of routine tasks. It sends emails when tasks need doing.

It beats a calendar for the following use cases of routine task:

1. Where tasks are low priority, so you don't want over-crowding calendar with stuff that doesn't matter a lot
2. When tasks don't need doing on any specific date/time
3. When tasks are to be done on your social platforms, and Houndr will be able to automatically detect whether you've done them or not.

Houndr is ideal for people who love to work using email, especially by flagging emails that need some kind of action. You may want to set up a mail rule to specially move or flag Houndr messages. As a side-note, if you use Google Calendar you may want to set it up to email each day with your agenda: this is a useful gcal feature that goes hand-in-hand with the email-driven philosophy of Houndr.

Tasks are configured in `config.xml`. A sample configuratation file is provided. The configuration file provides access details for various social platforms that may be scanned, as well as rules on what to email about.

Rules are set up to send emails based on a conjunction ('AND') of triggers.

Houndr was written by programmer(s) for programmers. It may become more user-friendly over time, but this is not designed as a consumer-facing product.

## System requirements ##

1. PHP 7+
2. SimpleXML PHP extension
3. cURL PHP extension
4. IMAP PHP extension (if the email social platform is used)
5. A working local SMTP server, for which PHP is configured

## Configuration syntax ##

The basic config file structure is self-explanatory.

The email settings, and timezone setting, are self-explanatory.

Each social platform has its own configuration settings. However, each must define a `platform-id` setting. This is because you can have multiple instantiations of a social platform (e.g. check multiple email accounts); rules don't directly reference the social platforms, they reference platform IDs.

Rule triggers can be configured as follows:

1. Daily frequency, `trigger-daily-frequency="n"`
2. Daily frequency range (randomized), `trigger-daily-frequency-range="n1-n2"`
3. Time of day, `trigger-time="HH:ii"`
4. Specific days, `trigger-days="[Monday,][Tuesday,][Wednesday,][Thursday,][Friday,][Saturday,][Sunday]"`
5. Lack of activity on a social platform over a day threshold, `trigger-daily-inactivity="n" trigger-daily-inactivity-scope="<comma-separated list of platform IDs>" trigger-daily-inactivity-filter="<social-platform-specific filter>"`; across the scopes if any has activity newer than n days then the rule will not be triggered
6. Flagged email that was received over an hourly threshold ago, `trigger-flagged-threshold="n" trigger-flagged-threshold-scope="<comma-separated list of platform IDs>" trigger-flagged-threshold-filter="<social-platform-specific filter>"`; across the scopes if any has a flagged email older than n days then the rule will be triggered

As you can see, some triggers allow filtering. Filters are interpreted as follows for the different social platforms supported:

1. Email, a comma-separate list of email name/address substrings to consider
2. Facebook, not implemented (as scanned activity is all public activity)
3. Minds (")
4. Twitter (")
5. RSS (")

All rules may be given the following properties, to define the email that is sent:

1. `email-subject` (will be combined with the globally configured subject-prefix and subject-suffix) (default: "nag-nag-nag")
2. `email-priority` (1 is highest priority, 5 is lowest) (default: "3")

If the rule has an XML body (i.e. is not an self-closing tag), that will be the body text of the email (all white-space is stripped from the start and end of lines).

If there is no XML body then the subject will also be the body.

Variables define substitutions that can occur in the subject or body of emails, via `{variable-name}`. They are useful for when you are writing multiple rule bodies that are very similar (e.g. individual daily task lists that overlap a lot but do vary a bit by day).

You can also define incrementors, like {+2}. This will start from 1 then increment by 2 (in this example) on each rule-pass run.

## Installation ##

Houndr is designed to run on your desktop computer, not a server.

Any use of the facebook social platform will need to be configured by loading `oauth.php?platform_id=<whatever>` into your web browser. This will trigger an oAuth to your Facebook account, and save the ID.

Test `cron.php` manually on the command line.

If you want to debug individual rules, assign them an `id` attribute, and call them with:

`php cron.php -o=some-rule-id`

'o' means only rule. You can comma-separate multiple rules here.

You can also use 's' with a comma-separated list of rules to skip.

Tie your system's Cron into the supplied cron.php file.

For example, for every 30 minutes:

`
0,30 * * * * php /home/example/houndr/cron.php
`

## Contributing ##

This project has been created to 'scratch an itch'.

Other contributors may want to (for example):

1. Add support for additional social platforms, such as Instagram
2. Add support for additional triggers
3. Make it more user-friendly, such as creating a management UI (so you don't need to hand configure an XML file)

Pull requests are welcomed.
